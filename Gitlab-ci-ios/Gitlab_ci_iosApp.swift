//
//  Gitlab_ci_iosApp.swift
//  Gitlab-ci-ios
//
//  Created by sy maymouna on 10/07/2023.
//

import SwiftUI

@main
struct Gitlab_ci_iosApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
